package at.patsch.bringsma.service.impl;

import at.patsch.bringsma.dto.ProductDto;
import at.patsch.bringsma.model.Product;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductServiceImplTest {

    @Autowired
    private ProductServiceImpl testee;

    @Test
    void givenListOfProducts_whenCallingGetProducts_shouldReturnProductDtos() {

        List<Product> products = createProducts();

        List<ProductDto> actualProducts = testee.getProducts();

        assertThat(actualProducts).hasSize(products.size());

    }

    private List<Product> createProducts(){
        return Arrays.asList(
                new Product("Käse", "Gouda"),
                new Product("Bier", "Stiegl"),
                new Product("Äpfel", null),
                new Product("Brot", "")
        );
    }
}