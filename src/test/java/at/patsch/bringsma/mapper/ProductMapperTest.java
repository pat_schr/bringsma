package at.patsch.bringsma.mapper;

import at.patsch.bringsma.dto.ProductDto;
import at.patsch.bringsma.model.Product;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductMapperTest {

    ProductMapper testee = ProductMapper.INSTANCE;

    @Test
    public void givenProduct_whenToDto_thenShouldMapCorrectly(){

        Product product = new Product("Bier", "Stiegl 330ml");

        ProductDto dto = testee.productToProductDto(product);

        assertThat(dto).isNotNull();
        assertThat(dto.getName()).isEqualTo(product.getName());
        assertThat(dto.getNotes()).isEqualTo(product.getNotes());
    }

}
