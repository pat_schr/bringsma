package at.patsch.bringsma.model;

import lombok.*;

@Data
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
public class Product {

    private final String name;
    private final String notes;
    private Boolean checked = false;


}
