package at.patsch.bringsma.service;

import at.patsch.bringsma.dto.ProductDto;

import java.util.List;

public interface ProductService {

    List<ProductDto> getProducts();

}
