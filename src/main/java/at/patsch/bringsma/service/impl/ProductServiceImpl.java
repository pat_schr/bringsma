package at.patsch.bringsma.service.impl;

import at.patsch.bringsma.dto.ProductDto;
import at.patsch.bringsma.mapper.ProductMapper;
import at.patsch.bringsma.model.Product;
import at.patsch.bringsma.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("productService")
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductMapper productMapper;

    private static final List<Product> DUMMY_PRODUCTS = Arrays.asList(
      new Product("Frischkäse", "Schnittlauch"),
      new Product("Karotten", "500g"),
      new Product("Joghurt", ""),
      new Product("Bier", "Stiegl 300ml"),
      new Product("Brot", "500g")
    );

    @Override
    public List<ProductDto> getProducts() {
        Product p = new Product("", "");
        p.getName();

        return productMapper.productsToProductDtos(DUMMY_PRODUCTS);
    }
}
