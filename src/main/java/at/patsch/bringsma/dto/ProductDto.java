package at.patsch.bringsma.dto;

import lombok.*;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode
public class ProductDto {

    private String name;
    private String notes;


}
