package at.patsch.bringsma.mapper;

import at.patsch.bringsma.dto.ProductDto;
import at.patsch.bringsma.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class ProductMapper {

    public static final ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    public abstract ProductDto productToProductDto(Product product);

    public abstract List<ProductDto> productsToProductDtos(List<Product> products);

}
