package at.patsch.bringsma.controller;

import at.patsch.bringsma.dto.ProductDto;
import at.patsch.bringsma.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(ProductController.API_PATH)
@RequiredArgsConstructor
public class ProductController {

    static final String API_PATH = "/api/products";

    private final ProductService productService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ProductDto>> list(){
        return new ResponseEntity<>(productService.getProducts(), HttpStatus.OK);
    }

}
