package at.patsch.bringsma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BringsmaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BringsmaApplication.class, args);
	}

}
